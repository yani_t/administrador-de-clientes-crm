# Administrador de clientes

En este proyecto se crea una aplicación registra y edita clientes con un cierto rol y empresa a la cual pertenecen similar al proyecto de administrador de pacientes de una veterinaria. Sin embargo, en el mismo se comienza a aplicar el concepto de vistas y con ello el uso de [Vue Router](https://es.vuejs.org/v2/guide/routing)	

El mismo es el proyecto 6 de 10 del curso de Vue.js 3 tomado en Udemy. El propósito de estos es formarme con las bases fundamentales de esta tecnologia aplicando de manera práctica a proyectos realistas.
